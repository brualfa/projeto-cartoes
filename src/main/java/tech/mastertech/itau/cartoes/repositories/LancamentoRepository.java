package tech.mastertech.itau.cartoes.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartoes.dto.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, String> {
	
}