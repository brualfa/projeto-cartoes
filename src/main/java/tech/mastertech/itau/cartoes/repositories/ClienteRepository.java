package tech.mastertech.itau.cartoes.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartoes.dto.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, String>{

}
