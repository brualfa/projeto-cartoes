package tech.mastertech.itau.cartoes.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartoes.dto.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, String> {
	
}
