package tech.mastertech.itau.cartoes.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Cartao {
	
	@Id
	private String numeroCartao;
	@ManyToOne
	private Cliente cliente;
	@ManyToOne
	private Bandeira bandeira;
	private boolean isAtivo;
	
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Bandeira getBandeira() {
		return bandeira;
	}
	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	public boolean getisAtivo() {
		return isAtivo;
	}
	public void setisAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
}
