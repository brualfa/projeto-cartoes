package tech.mastertech.itau.cartoes.dto;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Lancamento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idLancamento;
	@ManyToOne
	private Cartao cartao;
	private LocalDateTime dataLancamento;
	private String descricaoLancamento;
	private double valorLancamento;
	
	public long getIdLancamento() {
		return idLancamento;
	}
	public void setIdLancamento(long idLancamento) {
		this.idLancamento = idLancamento;
	}
	public Cartao getCartao() {
		return cartao;
	}
	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}
	
	public LocalDateTime getDataLancamento() {
		return dataLancamento;
	}
	public void setDataLancamento(LocalDateTime dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public String getDescricaoLancamento() {
		return descricaoLancamento;
	}
	public void setDescricaoLancamento(String descricaoLancamento) {
		this.descricaoLancamento = descricaoLancamento;
	}
	public double getValorLancamento() {
		return valorLancamento;
	}
	public void setValorLancamento(double valorLancamento) {
		this.valorLancamento = valorLancamento;
	}
	

}
