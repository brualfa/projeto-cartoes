package tech.mastertech.itau.cartoes.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartoes.dto.Bandeira;
import tech.mastertech.itau.cartoes.repositories.BandeiraRepository;

@Service
public class BandeiraService {

	@Autowired
	private BandeiraRepository bandeiraRepository;

	public Iterable<Bandeira> getBandeiras() {
		return bandeiraRepository.findAll();
	}

	public Bandeira getBandeira(int idBandeira) {

		Optional<Bandeira> bandeira = bandeiraRepository.findById(idBandeira);

		if (bandeira.isPresent()) {
			return bandeira.get();
		}

		return null;

	}

	public Bandeira setBandeira(Bandeira bandeira) {
		bandeiraRepository.save(bandeira);
		return bandeira;
	}

	public void deleteBandeira(int idBandeira) {

		bandeiraRepository.deleteById(idBandeira);

	}

	public Bandeira mudarNome(int idBandeira, String nome) {
		if (nome.equals("")) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nome inválido - Em branco");
		}

		Bandeira bandeira = getBandeira(idBandeira);

		if (bandeira != null) {
			bandeira.setNomeBandeira(nome);
			bandeiraRepository.save(bandeira);
		}

		return bandeira;

	}

}
