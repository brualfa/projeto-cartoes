package tech.mastertech.itau.cartoes.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartoes.dto.Cartao;
import tech.mastertech.itau.cartoes.repositories.CartaoRepository;

@Service
public class CartaoService {
	
	@Autowired
	private CartaoRepository cartaoRepository;
	
	//Retorna uma lista de cartões
	public Iterable<Cartao> getCartoes(){
		return cartaoRepository.findAll();
	}
	
	//Retorna um cartao
	public Cartao getCartao(String numeroCartao){
		
		Optional<Cartao> cartao = cartaoRepository.findById(numeroCartao);
		
		if (cartao.isPresent()) {
			return cartao.get();
		}
		return null;
	}
	
	//Cria um cartao
	public Cartao setCartao(Cartao cartao) {
		cartaoRepository.save(cartao);
		return cartao;
	}
	
	//Deleta um cartao
	public void deleteCartao(String cpfCartao) {
		cartaoRepository.deleteById(cpfCartao);
	}
	
	//Mudar status de cartao
	public Cartao mudarStatusCartao(String numeroCartao, boolean isAtivo) {
		
		Cartao cartao = getCartao(numeroCartao);
		
		if(cartao != null) {
			cartao.setisAtivo(isAtivo);
			cartaoRepository.save(cartao);
		}
		return cartao;
	}
}
