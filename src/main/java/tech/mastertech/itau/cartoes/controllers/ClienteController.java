package tech.mastertech.itau.cartoes.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartoes.dto.Cliente;
import tech.mastertech.itau.cartoes.services.ClienteService;

@RestController
@RequestMapping("/cartaocredito")
public class ClienteController {

	private Cliente retorno;

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/clientes")
	public Iterable<Cliente> getClientes() {

		return clienteService.getClientes();

	}
	
	@GetMapping("/cliente/{cpfCliente}")
	public Cliente getCliente(@PathVariable String cpfCliente) {
		retorno = clienteService.getCliente(cpfCliente);
		if(retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}
	
	@PostMapping("/cliente")
	public Cliente setCliente(@RequestBody Cliente cliente) {
		return clienteService.setCliente(cliente);
	}
	
	@DeleteMapping("/cliente/{cpfCliente}")
	public String deleteCliente(@PathVariable String cpfCliente) {
		clienteService.deleteCliente(cpfCliente);
		
		return "Solicitacao atendida";

	}
	
	@PatchMapping("/cliente/{cpfCliente}")
	public Cliente mudarDados(@PathVariable String cpfCliente, @RequestBody Map<String, String> dados) {
		
		String telefoneCliente;
		
		telefoneCliente = dados.get("telefoneCliente");
		
		if(!telefoneCliente.equals("")) {
			
			retorno = clienteService.mudarTelefone(cpfCliente, telefoneCliente);
		}
		
		
		if(retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}

}
