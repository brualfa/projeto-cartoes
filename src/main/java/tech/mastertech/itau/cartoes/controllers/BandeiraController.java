package tech.mastertech.itau.cartoes.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartoes.dto.Bandeira;
import tech.mastertech.itau.cartoes.services.BandeiraService;

@RestController
@RequestMapping("/cartaocredito")
public class BandeiraController {

	private Bandeira retorno;

	@Autowired
	private BandeiraService bandeiraService;

	@GetMapping("/bandeiras")
	public Iterable<Bandeira> getBandeiras() {

		return bandeiraService.getBandeiras();

	}

	@GetMapping("/bandeira/{idBandeira}")
	public Bandeira getBandeira(@PathVariable int idBandeira) {
		retorno = bandeiraService.getBandeira(idBandeira);
		if (retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}

	@PostMapping("/bandeira")
	public Bandeira setBandeira(@RequestBody Bandeira bandeira) {
		return bandeiraService.setBandeira(bandeira);
	}

	@DeleteMapping("/bandeira/{idBandeira}")
	public String deleteBandeira(@PathVariable int idBandeira) {
		bandeiraService.deleteBandeira(idBandeira);

		return "Solicitacao atendida";

	}

	@PatchMapping("/bandeira/{idBandeira}")
	public Bandeira mudarNome(@PathVariable int idBandeira, @RequestBody Map<String, String> nome) {
		retorno = bandeiraService.mudarNome(idBandeira, nome.get("nomeBandeira"));
		if (retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}

}
