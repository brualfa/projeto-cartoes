package tech.mastertech.itau.cartoes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cartoes.dto.Cartao;
import tech.mastertech.itau.cartoes.dto.Lancamento;
import tech.mastertech.itau.cartoes.services.CartaoService;
import tech.mastertech.itau.cartoes.services.LancamentoService;

@RestController
@RequestMapping("/cartaocredito")
public class LancamentoController {

//	private Lancamento retorno;
	
	@Autowired
	private LancamentoService lancamentoService;
	
	@Autowired
	private CartaoService cartaoService;
	
	@GetMapping("/lancamentos")
	public Iterable<Lancamento> getLancamento(){
		
		return lancamentoService.getLancamentos();
	}
	
	@PostMapping("/lancamento")
	public Lancamento setLancamento(@RequestBody Lancamento lancamento) {
		
		Cartao cartao = new Cartao();
		
		cartao = lancamento.getCartao();
		
		cartao = cartaoService.getCartao(cartao.getNumeroCartao());
		
		return lancamentoService.setLancamento(lancamento,cartao);
	}
	
}
